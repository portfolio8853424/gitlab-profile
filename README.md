# Hallo, ich bin Nikolai Junker 👋

Ich bin ein Wirtschaftsinformatik-Masterstudent im 4 Semester.

## Meine Interessen 👀 

Ich habe ein breites Spektrum an Interessen in der Informatik, das von maschinellem Lernen und Deep Learning bis hin zu Simulationen reicht. Darüber hinaus habe ich auch Erfahrung und Interesse an ABAP-Programmierung und SAP-Themen.

## Aktuelle Projekte 🌱 

Aktuell schreibe ich meine Masterarbeit.

## Zusammenarbeit 💞️ 

Ich bin immer auf der Suche nach spannenden Projekten und freue mich auf die Zusammenarbeit an praktischen Anwendungen, die eine technische Herausforderung darstellen.

## Kontakt 📫 

Einfach über GitLab Kontakt aufnehmen.